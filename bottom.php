<?php
    if(empty($_SESSION['email'])){
?>
<section style="margin-top:0" class="footer bg-dark">
    <div class="container container-fluid">
        <div class="row">
            <div class="col-md-8">
                <div style="padding:10px;">
                    <h5>เกี่ยวกับเรา</h5>
                    <p>วสหกวสหกวหสกดาวสหกาดวสกาดวาหกวดส</p>
                </div>
            </div>
            <div class="col-md-4">
                <div style="padding:10px;">
                    <p><i class="fab fa-facebook-square"></i> : Taladchaoban
                        <br> <i class="fas fa-envelope"></i> : taladchaoban@gmail.com
                        <br> <i class="fas fa-user"></i> : Poramin Attane</p>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    }
?>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/main.js"></script>
</body>

</html>