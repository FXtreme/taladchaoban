<?php
    $title="หน้าแรก | ตลาดชาวบ้าน";
    include_once("top.php");
?>
    <div class="container-fluid ">
        <div class="row flex-xl-nowrap ">
            <div class="col-12 col-md-2 d-none d-md-block sidebar">
                <div>
                    <form>
                        <div class="form-group">
                            <select class="form-control">
                                <option>เลือกประเภทสินค้า</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                                <option>เลือกชนิดสินค้า</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="form-control">
                                <option>สถานที่</option>
                            </select>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <input type="text" class="form-control" placeholder="ราคาเริ่ม" title="ราคาเริ่มต้น" /> </div>
                            <div class="col">
                                <input type="text" class="form-control" placeholder="สูงสุด" title="ราคาสูงสุด" /> </div>
                            <div class="w-100" style="margin-bottom:5px;"></div>
                            <div class="col">
                                <button class="btn btn-block btn-primary">ค้นหา</button>
                            </div>
                        </div>
                        <hr>
                        <div class="row text-center">
                            <div class="col-6">
                                <button class="btn btn-block btn-success">ซื้อ</button>
                            </div>
                            <div class="col-6">
                                <button class="btn btn-block btn-danger">ขาย</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-10" style="background-color:#e0e0e0">
                <div class="first-section main">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 margin-card">
                            <div class="card h-100">
                                <figure class="fig-img"> <img class="card-img" src="img/Citrus_Trees.jpg" alt="Card image cap"> </figure>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><img class="img-profile rounded-circle">
                                        <blockquote class="blockquote">
                                            Lorem ipsum dolor sit amet
                                            <footer class="blockquote-footer"><cite title="Source Title">3 นาทีที่แล้ว</cite></footer>
                                        </blockquote>
                                    </li>
                                </ul>
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5> <small class="text-muted"><i class="fas fa-bullhorn"></i> <span class="badge badge-success">เสนอซื้อ</span><br><i class="fas fa-map-marker-alt"></i> 654654654654<br><i class="fas fa-dollar-sign"></i> ราคา</small>
                                    <blockquote class="blockquote mb-0">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                                        <small>จำนวนผู้สนใจ : </small>
                                    </blockquote>
                                </div>
                                <div class="card-footer">
                                    <div class="row text-center text-muted">
                                        <div class="col"> <a href="#" class="btn btn-sm btn-block text-secondary"><i class="fas fa-star"></i> สนใจ</a> </div>
                                        <div class="col"> <a href="#" class="btn btn-sm text-secondary"><i class="fas fa-comment"></i> ความคิดเห็น</a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <div class="col-md-4 col-sm-6 margin-card">
                            <div class="card h-100">
                                <figure class="fig-img"> <img class="card-img" src="img/agriculture-1822446_1920.jpg" alt="Card image cap"> </figure>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item"><img class="img-profile rounded-circle">
                                        <blockquote class="blockquote">
                                            Lorem ipsum dolor sit amet
                                            <footer class="blockquote-footer"><cite title="Source Title">ได้รับการสนับสนุน</cite></footer>
                                        </blockquote>
                                    </li>
                                </ul>
                                <div class="card-body">
                                    <h5 class="card-title">Card title</h5> <small class="text-muted"><i class="fas fa-bullhorn"></i> <span class="badge badge-danger">เสนอขาย</span><br><i class="fas fa-map-marker-alt"></i> 654654654654<br><i class="fas fa-dollar-sign"></i> ราคา</small>
                                    <blockquote class="blockquote mb-0">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipias;ll; podfigpodktmn dlkfsdlkgk.</p>
                                        <small>จำนวนผู้สนใจ : </small>
                                    </blockquote>
                                </div>
                                <div class="card-footer">
                                    <div class="row text-center text-muted">
                                        <div class="col"> <a href="#" class="btn btn-sm btn-block text-secondary"><i class="fas fa-star"></i> สนใจ</a> </div>
                                        <div class="col"> <a href="#" class="btn btn-sm text-secondary"><i class="fas fa-comment"></i> ความคิดเห็น</a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    include_once("bottom.php");
?>