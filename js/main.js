$(document).ready(function () {
    $('#forget_password').hide();
});

function main_login() {
    let email = $('#main_email').val();
    let password = $('#main_password').val();
    $('#main_btn_login').attr("disabled", true);
    $('#main_btn_login').text("Loading");
    $.post("model/login.php", {
        email: email
        , password: password
    }, function (data) {
        if (data == 'success') {
            window.location.assign("main.php");
        }
        else if (data == 'fail') {
            $('#main_btn_login').attr("disabled", false);
            $('#main_btn_login').text("เข้าสู่ระบบ");
            $('#login_notice').text('ชื่อผู้ใช้งานหรือรหัสผ่านไม่ถูกต้อง');
        }
        else {
            $('#login_notice').text('มีปัญหาในการส่งข้อมูล');
            $('#main_btn_login').attr("disabled", false);
            $('#main_btn_login').text("เข้าสู่ระบบ");
        }
    });
    return false;
}

function forget_password() {
    $('#main_login').fadeOut('slow');
    $('#forget_password').fadeIn();
}

function send_forget_password() {
    let email=$('#send_to_email').val();
    $('#btn_reset_password').text("Loading");
    $('#btn_reset_password').attr('disabled',true)
    $.post('model/sent_mail.php',{
        email:email
    },function(data){
        if(data=='success'){
            $('#btn_reset_password').hide();
            $('#reset_notice').html("<p class='text-left'>เราได้ทำการส่งลิงค์ไปที่อีเมลล์ของท่านแล้ว หากไม่พบกรุณาคลิก<a href='#' onclick='return send_forget_password();'>ที่นี่</a> เพื่อส่งใหม่อีกครั้ง</p>");
        }else if(data=='invalid_email'){
            $('#reset_notice').text('ไม่มีอีเมลล์นี้');
            $('#btn_reset_password').text("รีเซ็ทรหัสผ่าน");
            $('#btn_reset_password').attr('disabled',false)
        }else{
            $('#reset_notice').text('เกิดปัญหาในการส่งข้อมูล');
            $('#btn_reset_password').text("รีเซ็ทรหัสผ่าน");
            $('#btn_reset_password').attr('disabled',false)
        }
        console.log(data);
        
    });
    return false;
}

function register() {
    let fname = $('#regis_firstname').val();
    let lname = $('#regis_lastname').val();
    let email = $('#regis_email').val();
    let password = $('#regis_password').val();
    let re_password = $('#regis_repassword').val();
    let date = $('#regis_birthday').val();
    let intdate = new Date(date).getTime();
    let intdate_convert = intdate / 1000;
    if (password != re_password) {
        $('#repassword_help').text('รหัสผ่านไม่ตรงกัน');
        return false;
    }
    else {
        $('#btn_register').attr('disabled', true);
        $('#btn_register').text("Loading");
        $.post("model/register.php", {
            fname: fname
            , lname: lname
            , email: email
            , password: password
            , birthday: intdate_convert
        }, function (data) {
            if (data == 'success') {
                window.location.assign("main.php");
            }
            else if (data == 'fail') {
                $('#btn_register').attr('disabled', false);
                $('#btn_register').text("ลงทะเบียน");
                $('#regis_notice').text("อีเมลล์นี้ถูกใช้งานแล้ว");
            }
            else {
                $('#btn_register').attr('disabled', false);
                $('#btn_register').text("ลงทะเบียน");
                $('#regis_notice').text("มีัญหากับการเชื่อมต่อ");
            }
        });
    }
    return false;
}

function send_reset_password(){
   
    let new_password=$('#new_password').val();
    let re_new_password=$('#re_new_password').val();
    let email=$('#email').val();
    let hash=$('#hash').val();
    $("#btn_reset_password").attr("disabled",true);
    $("#btn_reset_password").text("Loading");
    if(new_password!=re_new_password){
        $('#re_new_password').addClass("is-invalid");
    }
    $.post("model/reset_pass_model.php",
        {
            email:email,
            new_password:new_password,
            hash:hash,
        }
    ,
    function(data){
        console.log(data);
        $("#btn_reset_password").attr("disabled",false);
        $("#btn_reset_password").text("รีเซ็ทรหัสผ่าน");
        if(data=='success'){
            window.location.assign("loginpage.php");
        }else if(data=='same password'){
            $('#new_password').addClass('is-invalid');
        }else{
            $('#reset_notice').text("มีปัญหากับการเชื่อต่อ");
        }
    });


    return false;
}