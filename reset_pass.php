<?php
    $title="เปลี่ยนรหัสผ่าน";
    include_once("top.php");
    if(!$_GET){
        header("location:index.php");
    }else{
        include_once("model/config.inc.php");
        $sql="select * from hash_link where hash='{$_GET['hash']}'";
        $result=$conn->query($sql);
        $row=$result->num_rows;
        if($row>=1){
            $dbarr=$result->fetch_assoc();
            if($dbarr['type']!='forgetpassword' || $dbarr['status']!=0){
                header("location:index.php");
            }
        }else{
            header("location:index.php");
        }
        
    }
?>
<section id="reset_password" class="first-section" style="min-height:500px;padding-top:70px;">
        <div class="container container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-md-4">
                    <div class="panel">
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <h5 class="text-center">เปลี่ยนรหัสผ่าน</h5>
                                <hr>
                            </div>
                        </div>
                        <form onsubmit="return send_reset_password();">
                        <input type="้text" class="form-control" id="email" value="<?=$dbarr['member_email']?>" hidden="hidden">
                        <input type="้text" class="form-control" id="hash" value="<?=$_GET['hash']?>" hidden="hidden">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group mb-2">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fas fa-key"></i></div>
                                            </div>
                                            <input type="password" class="form-control" id="new_password" placeholder="รหัสผ่านใหม่" required>
                                            <div class="invalid-feedback">รหัสผ่านเดิม</div>  
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group mb-2">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fas fa-key"></i></div>
                                            </div>
                                            <input type="password" class="form-control" id="re_new_password" placeholder="ยืนยันรหัสผ่านใหม่">
                                            <div class="invalid-feedback">รหัสผ่านไม่ตรงกัน</div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary" id="btn_reset_password">รีเซ็ทรหัสผ่าน</button><br>
                                    <small id="reset_notice" class="text-danger"></small>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
    include_once("bottom.php");
?>