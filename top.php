<?php
    session_start();
    include_once("model/config.inc.php");
    $host_prefix="/taladchaoban/";
//    session_destroy();
    if(isset($_SESSION['email'])){
        if($_SERVER['REQUEST_URI']=="{$host_prefix}index.php" || $_SERVER['REQUEST_URI']=="{$host_prefix}loginpage.php" || $_SERVER['REQUEST_URI']==$host_prefix){
            header("location:main.php");
        }
        $sql="select * from member where email='{$_SESSION['email']}'";
        if($result=$conn->query($sql));
        {
            $dbarr=$result->fetch_array();
        }
    }

?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Kanit" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>
        <?=$title?>
    </title>
</head>

<body>
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark"> <a class="navbar-brand" href="index.php" style="margin:8px;">TaladChaoban</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <?php
        if(isset($_SESSION['email'])){
    ?>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#" title="เพิ่มประกาศ"><i class="fas fa-plus-circle"></i> เพิ่มประกาศ</a>
          </li>
            <li class="nav-item active">
            <a class="nav-link" href="#" title="แจ้งเตือน"><i class="fas fa-bell"><span class="badge badge-danger">4</span></i> แจ้งเตือน</a>
          </li>
            <li class="nav-item active">
            <a class="nav-link" href="#" title="แจ้งเตือน"><i class="fas fa-comments"><span class="badge badge-danger">4</span></i> ข้อความ</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img class="nav-img-profile rounded-circle">  <?php echo $dbarr['fname']." ".$dbarr['lname'] ?>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">โปรไฟล์</a>
              <a class="dropdown-item" href="#">ตั้งค่า</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="model/logout.php">ออกจากระบบ</a>
            </div>
          </li>
        </ul>
    <?php
        }else{
    ?>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item"> <a class="nav-link active" href="index.php#aboutus"><i class="fas fa-question-circle"></i> วิธีการใช้งาน</a> </li>
            <li class="nav-item"> <a class="nav-link active" href="index.php#aboutus"><i class="fas fa-address-card"></i> เกี่ยวกับเรา</a> </li>
            <li class="nav-item"> <a class="nav-link active" href="main.php"><i class="fas fa-bullhorn"></i> ตลาด</a> </li>
            <li class="nav-item"> <a class="nav-link active" href="loginpage.php"><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</a> </li>
        </ul>
    <?php
        }
    ?>
        </div>
    </nav>