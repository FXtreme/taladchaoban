<?php
    $title="เข้าสู่ระบบ | ตลาดชาวบ้าน";
    include("top.php");
?>
    <section id="main_login" class="first-section" style="min-height:500px;padding-top:70px;">
        <div class="container container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-md-4">
                    <div class="panel">
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <h5 class="text-center">เข้าสู่ระบบ</h5><hr> </div>
                        </div>
                        <form onsubmit="return main_login();">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                                        </div>
                                        <input type="email" class="form-control" id="main_email" placeholder="อีเมลล์" required> </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fas fa-key"></i></div>
                                        </div>
                                        <input type="password" class="form-control" id="main_password" placeholder="รหัสผ่าน" required> </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary" id="main_btn_login">เข้าสู่ระบบ</button> | <a href="#" onclick="return forget_password();">ลืมรหัสผ่าน</a> </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 text-center"> <small id="login_notice" class="text-danger"></small> </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="forget_password" class="first-section" style="min-height:500px;padding-top:70px;">
        <div class="container container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-md-4">
                    <div class="panel">
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <h5 class="text-center">ลืมรหัสผ่าน</h5>
                                <hr>
                                <p class="text-left">กรุณากรอกอีเมลล์ของท่านเพื่อเข้าสู่ขั้นตอนการรีเซ็ตรหัสผ่าน</p>
                            </div>
                        </div>
                        <form onsubmit="return send_forget_password();">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <div class="input-group mb-2">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text"><i class="fas fa-envelope"></i></div>
                                            </div>
                                            <input type="email" class="form-control" id="send_to_email" placeholder="อีเมลล์" required> </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary" id="btn_reset_password">รีเซ็ทรหัสผ่าน</button><br>
                                    <small id="reset_notice" class="text-danger"></small>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    include("bottom.php");
?>