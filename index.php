<?php
    $title="ตลาดชาวบ้าน";
    include("top.php");
?>
    <section id="home">
        <div class="container container-fluid">
            <div class="row" style="padding-bottom:10px">
                <div class="col-md-6">
                    <div class="home-head text-center">
                        <h2 class="display-5"><mark>ตลาดสินค้าสำหรับผู้ผลิต</mark></h2>
                        <p class="lead"><mark>รายละเอียดย่อย</mark></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel" style="margin-top:50px">
                        <h3 class="text-center">สมัครสมาชิก</h3>
                        <form id="formregister" onsubmit="return register();">
                            <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="regis_firstname">ชื่อ</label>
                                <input type="text" class="form-control" id="regis_firstname" placeholder="ชื่อผู้ใช้งาน" required> </div>
                            <div class="form-group col-md-6">
                                <label for="regis_lastname">นามสกุล</label>
                                <input type="text" class="form-control" id="regis_lastname" placeholder="นามสกุล" required> </div>
                            </div>
                            <div class="form-group">
                                <label for="regis_email">อีเมลล์</label>
                                <input type="email" class="form-control" id="regis_email" aria-describedby="emailHelp" placeholder="กรุณากรอกอีเมลล์" required> <small id="emailHelp" class="form-text text-muted">อีเมลล์ของท่านจะถูกเก็บเป็นความลับ</small> </div>
                            <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="regis_password">รหัสผ่าน</label>
                                <input type="password" class="form-control" id="regis_password" placeholder="รหัสผ่าน" required></div>
                            <div class="form-group col-md-6">
                                <label for="regis_repassword">ยืนยันรหัสผ่าน</label>
                                <input type="password" class="form-control" id="regis_repassword" placeholder="กรุณายืนยันรหัสผ่าน" required><small id="repassword_help" class="form-text text-danger"></small></div>
                            </div>
                            <div class="form-group">
                                <label for="regis_birthday">วันเกิด</label>
                                <input type="date" class="form-control" id="regis_birthday" aria-describedby="emailHelp" required></div>
                            <div class="form-group">
                                <p class="small">เมื่อคลิก ลงทะเบียน แสดงว่าคุณยินยอมตามข้อกำหนดและคุณได้อ่านนโยบายข้อมูลของเราแล้ว รวมถึงการใช้คุกกี้</p>
                                <button class="btn btn-info btn-lg" type="submit" id="btn_register">ลงทะเบียน</button>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 text-center"> <small id="regis_notice" class="text-danger"></small> </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="underhome">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-4 text-center">
                    <h1 class="display-4"><i class="fas fa-map-marker-alt"></i></h1>
                    <p>ไม่มีสถานที่</p>
                </div>
                <div class="col-md-4 text-center">
                    <h1 class="display-4"><i class="fas fa-bullhorn"></i></i></h1>
                    <p>ประกาศ</p>
                </div>
                <div class="col-md-4 text-center">
                    <h1 class="display-4"><i class="fas fa-comments"></i></i></h1>
                    <p>พูดคุยเสนอแลกเปลี่ยน</p>
                </div>
            </div>
        </div>
    </section>
    <?php
    include("bottom.php");
?>